FROM nixpkgs/nix-flakes:latest AS builder
# Copy our source and setup our working dir.

COPY . /app
WORKDIR /app

# VOLUME ./nix /etc/nix

RUN nix-channel --update

ARG NIX_PATH=nixpkgs=channel:nixos-unstable

# copy keys from keys folder
RUN mkdir -p ~/.ssh && \
    cp /app/keys/* ~/.ssh/ && \
    chmod 600 ~/.ssh/*

# HACK: Use this if you are having host key errors
# the better hack is to add a known-hosts file with the correct hosts to keys/
# RUN echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config

# makes nb happy
ENV EDITOR=nano
ENV TMPDIR=/app/tmp

# Cache some requirements in /nix/store
# RUN echo "extra-experimental-features = nix-command flakes" >> /etc/nix/nix.conf
# RUN nix develop -c ,setup 
RUN nix-shell -p nb git --run ./,setup.sh


#  RUN nix-build -A pythonFull '<nixpkgs>'
CMD nix develop -c rollout 
