# script that is run on the docker container
# specifically not in nix
# this is to avoid running a nix command in a docker build
# running nix build in a building container will bloat /var/lib/docker/overlay2 with CoW copies 
# i.e. no docker command can clean these CoW copies
# the docker solution is to use volumes 
# https://docs.docker.com/engine/storage/drivers/ for more info
#
# later... we will have our mounted shared /nix from docker-compose
# so it needs to be a separate script
git config --global user.email bot@notarealemail.com
git config --global user.name bot
git clone git@gitlab.com:slenderq/nb-ai.git -b main "$HOME/.nb/ai"
nb use ai
