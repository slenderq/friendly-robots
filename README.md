# friendly-robots

Swarm of Self Hosted LLM-based agents enclosed in Docker containers that perform small development tasks that accumulate.

The goal being to utilze cheap Ollama agents to do a lot of dumb work.

This is different than the usual "use AI to automate the high value work"

These Agents are looking to do lots of "dumb" tasks quickly. 

## Features

- Fully Reproducable in Nix
- Ollama Integration
    - Adaptor for future model expantjion 
- Agent Document reading.
- Agent Memory
- Contract class for LLM file reading interface
- Simple RAG via `nb` command line tool
- Docker Support

## Concept

### Contracts

Contracts are the main way to control the swarm of agents.

The logic for them are stored in `contract.py`

Agents add `bills` to contracts if they have not tried the task. 

A contract has a file in a specific git repository to use as input.

A contract has a specific task do based on the content of that file

#### Contract Syntax

Here is an example minimal contract

```
# contract gather
goal = "Create a complete documentation entry on the file"
expected_output_content = "a notebook entry with title and content keys"
patch_output = ""
targetFile = "flake.nix"
targetTool = "nb"
repo = "https://gitlab.com/slenderq/friendly-robots.git"
bot = []

[settings]
reusable = true
skip_mr = false
```

More example contracts can be found in `contracts/`

#### Contract Loading

```
nb ai:new 
[paste in your contract]
```

#### Proccessing Contracts

There is a special bot called the `_PublisherBot` that will combine contracts

It also "recombines" them to create new solutions from multiple bots.



## Requirements
- Ollama https://ollama.com/
    - running with `ollama serve`
- A `nb` git repository and a ssh key named id_ed25519 that grants write access to it in the keys/ folder.


## Setup

### NixOS 

```
nix develop -c rollout
```

Use the `$BOT_*` Environment variables from the docker-compose file to control behavior



### Linting / Tests

```
docker-compose up ollama database -d
nix develop -c ,validate
```

### Adding Poetry pkgs

```
nix-shell -p poetry
poetry add <pkg>
git commit -u # commit the poetry updates
nix develop # pray it builds 
```
#### It didn't build...
Many packages just plainly don't work out of the box with poetry2nix. Often requiring extra Nix to make it work. Often this is worth it but not an easy task.
https://github.com/nix-community/poetry2nix/blob/master/docs/edgecases.md

```
# get out of nix building and just trust python wheels
# this has it OWN issues sometimes and its not a silver bullet 
preferWheels = true;
```

### Docker 
#### Running all the bots

`_PublisherBot` is a good bot to monitor progress from
```
dc build && dc up -d && dc logs publisher -f
```
#### SSH key initial bootstrap

```
cp ~/.ssh/id_ed25519* keys/ # id_ed25519 is the assumed filename for the key pair
nix develop -c ,encrypt
git add keys/id_ed25519.gpg
```

#### Ollama models bootstrap

We need to make sure the ollama instance we spun up has all the models it needs ahead of time.

```
nix develop
docker compose up ollama -d 
export OLLAMA_URL=http://localhost:11434
,test_models
```

If any models are missing ollama will download them for you. 

#### Database updates

```
nvim init.sql
dc down database -v
dc up database -d 
```

#### Building / Running

```
nix develop 
# password encyrpted secrets, set your own with ,encrypt
# see the ,encrypt script in flake.nix for more info
,decrypt 
docker-compose build 
docker-compose up
```

##### Host key errors

Ensure that the `keys/known_hosts` file is up to date to ensure host verification is still working

##### Disk space errors
The Nix tmpdir volume can fill up to be quite big if you build many different versions of the container.

The following commands will purge all unused volumes. 

```
docker-compose down
docker system volume prune
```

- `TODO:` Find a shared nix store or something to prevent the temp volume from filling up. 
    - if we are using the nix store of the host system, we will likely have everything we need so no building required.
    - this arguably makes the nix use case make more sense as you get a lot of benefits.

- https://forums.docker.com/t/some-way-to-clean-up-identify-contents-of-var-lib-docker-overlay/30604
    - Note that development workloads grow `/var/lib/docker/overlay2` with repeated builds pretty quickly
    - another reason why a bind mount to the nix store on the server 

#### Creating a swarm of bots

The personallity of the bots is controlled by the `BOT_NAME` and `BOT_VALUES` environment vars.

### Wait, what's Nix?!? 

This guide is a very good intro with lots of different media about nixos.
https://github.com/mikeroyal/NixOS-Guide

#### TLDR;
Highly reproducable and flexable develpoment environment that I can very easily put in a docker container.


## Resources
- https://mitchellh.com/writing/nix-with-dockerfiles
- [More Agents Is All You Need](https://arxiv.org/pdf/2402.05120)
- https://medium.com/@pradeepgoel/demystifying-embeddings-b680e095b145
- https://www.strangeloopcanon.com/p/evaluations-are-all-we-need
- this is probably way better memory: https://github.com/kingjulio8238/memary
- https://kevincox.ca/2022/01/02/nix-in-docker-caching/
- https://github.com/pgvector/pgvector-python?tab=readme-ov-file#psycopg-3

## Further Work/TODOs
- Use non-default port for the database



## Algorithm
### Abstract Bot

### Values Statement
The bot receives a set of objectives that it knows it needs to accomplish, referred to as V.

#### Memory
The bot operates with a string-based memory, known as M, which is a significant asset. Compression is also a viable option.

### Contracts
Contracts represent the tasks we aim to complete. They are the actionable part of the prompt and are structured in TOML format. For instance, finding an enhancement in the codebase. Clear outputs and contracts are Pydantic data. Bots commit to the contract, and the contract's prompt is loaded. These contracts are referred to as C.

contracts can be made to have lots of different properties

i.e. repeatable

contracts have stamps from bots when the start them, this is a commit action that is done via gitlab

contracts are for specific files

contracts can be created by bots 

i.e. there is a initial contract 

Automaton dest: data about which Automaton its going to when its done 

some contracts are allowed to 

### Automaton
The 'receiver' of the contracts. It executes actions based on the contract. Utilizes GitLab.

part of the automonton manages the lifecycle of the contracts. 

when a PR is merged from a completed the contract is removed unless it has the renew flag

There is another one that actually creates other contracts 



# selecting contracts?
I think they should grab contracts at random

# dead contracts? what if a contract is too hard? 
bots have a limit of how many llm calls have they per contract,

this limit can be raised in a contract as need

when they are at their limit, they will create a PR with a dump of their memory 

### Voting System
We can implement a voting system based on the method described in this paper: https://arxiv.org/pdf/2402.05120. The similarity score can be used to select bills or determine the action to be taken.

For potential implementation of the similarity score:
- https://pypi.org/project/bleu/
- Embeddings similarity may be an interesting metric to explore 


### Actions
M + V + C
Based on your Values, Existing Memory, and the Contract, 

Fill memory, 
Using V, and C and F-slice (the file the contract asks for) 
Extract content from the file that will aid your contract goal. 


We will call this piece of info M*

Based on M, integrate M*. If the existing structure that makes sense for the info, keep it. 

If M* contains info that means the structure of the knowledge in M must change, then edit it and change it keeping all the information.

Rephrase to be minimal as possible to save memory space


Fill memory until you have either:

Enough information to accomplish my goal? 

OR 

A full memory 

OR 
scanned the file n-limit number of times 

(maybe) enough information that I want to change the file that I'm scanning 




If so fill out the information that the contract requests based on your memory

This will likely be asking for a specific patch file of the change that you want to make 

I may have to find some way to make this is a valid patch and imo will be a pain

After some likely patch validation.

Ask Values and Contract, and Patch if this is a good solution to the contract.

If so fill in the meta section of the contract, which contains the PR info and describes the change and why it works 


If yes, complete the contract. There is a clear signal from the LLM and special prompt case for it, 

If any of these failed, go back to the Memory and either add to it based on the feedback or remove details or ideas from it. 

Repeat until goal is done or we are out of llm actions


## Search

Find more information.

Abstractly:

A = M + V + C

Purely functional with side effects. A bot will take its data and output data. It can also take actions like text selection.

### Bot Types

#### Search Bot
Bots that find potential inputs.

#### Bots Triggering Actions

How should I model the bots taking actions in the world?

### Contracts
These are TOML files containing prompts that define the agent's behavior. They include conditions.

### Utilizing GitLab for Operations
Refer to https://python-gitlab.readthedocs.io/en/stable/gl_objects/merge_requests.html for comprehensive GitLab operations.

### Token Acquisition
A token is required to interact with the library.


# Future ideas
## Use pgVector to store notes on files 
