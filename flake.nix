{
  description = "friendly_robots package managed with poetry2nix";

  inputs = {
    nixpkgs = { url = "github:nixos/nixpkgs/nixpkgs-unstable"; };
    flake-utils = { url = "github:numtide/flake-utils"; };
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };


    compose2nix = {
      url = "github:aksiksi/compose2nix";

      inputs.nixpkgs.follows = "nixpkgs";
    };

  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        inherit (poetry2nix.lib.mkPoetry2Nix { inherit pkgs; }) mkPoetryApplication mkPoetryEnv defaultPoetryOverrides;
        python = pkgs.python311Full;
        pythonPkgs = pkgs.python311Packages;


        pypkgs-build-requirements = {
          ollama = [ "poetry" ];
          numpy = [ "poetry" ];
        };
        poetryOverrides = defaultPoetryOverrides.extend (self: super:
          builtins.mapAttrs
            (package: build-requirements:
              (builtins.getAttr package super).overridePythonAttrs (old: {
                buildInputs = (old.buildInputs or [ ]) ++ (builtins.map (pkg: if builtins.isString pkg then builtins.getAttr pkg super else pkg) build-requirements);
              })
            )
            pypkgs-build-requirements
        );
        appEnv = mkPoetryEnv {
          projectDir = self;
          editablePackageSources = {
            friendly-robots = ./src;
          };
          overrides = poetryOverrides;
          # set to false for a poetry2nix contrubtion
          preferWheels = true;
        };
        appPkgs = with pkgs; [
          poetry
          pdm
          nixpkgs-fmt
          nb
          glab
          remarshal
          ollama
          gnupg
          ps
          postgresql
        ];
        customPkgs = [
          self.inputs.compose2nix.packages.${system}.default
        ];
        customScripts = [
          (pkgs.writeShellApplication {
            name = ",validate";
            text = ''
              #!/usr/bin/env bash
              set -e
              # convert the docker-compose from yaml to toml because f yaml
              # toml-cli toml2yaml config.toml > config.yaml
              nixpkgs-fmt .
              git add -u -p
              black .
              git add -u -p
              pytest --lf
            '';
          })
          (pkgs.writeShellApplication {
            name = ",build_docker";
            text = ''
              #!/usr/bin/env bash
              set -e
              # toml2yaml docker-compose.toml docker-compose.yml
              git add -u -p
              docker compose build 
            '';
          })
          (pkgs.writeShellApplication {
            name = ",start";
            text = ''
              #!/usr/bin/env bash
              set -e
              ,setup
              rollout
            '';
          })
          (pkgs.writeShellApplication {
            name = ",setup";
            text = ''
              #!/usr/bin/env bash
              set -e
              # ,test_models
              ,git_setup # only run on container
              ,nb_setup
            '';
          })
          (pkgs.writeShellApplication {
            name = ",test_models";
            text = ''
              #!/usr/bin/env bash
              # Occasionally, the llama model needs to be updated before executing the robots.
              # This command triggers the model on the server process instead of the current process.
              # This command should be run for every possible model
              # Respects the OLLAMA_HOST env var defined in docker-compose
              ollama run codellama:7b "Prove you are working correctly" 
              ollama run dolphin-mistral:7b "Prove you are working correctly" 
              ollama run zephyr:7b "Prove you are working correctly" 
              ollama run llama3.1:8b "Prove you are working correctly" 
              ollama run mistral:7b "Prove you are working correctly" 
              ollama run qwen2:7b "Prove you are working correctly" 
              ollama run starcoder2:7b "Prove you are working correctly" 
              ollama run openorca:7b "Prove you are working correctly" 
              # ollama run nomic-embed-text "Prove you are working correctly" 

            '';

          })
          (pkgs.writeShellApplication {
            name = ",build";
            text = ''
              #!/usr/bin/env bash
              set -e
              nix develop -c ,validate
              nix develop -c ,build_docker
            '';
          })
          (pkgs.writeShellApplication {
            name = ",nb_setup";
            text = ''
              #!/usr/bin/env bash
              git clone git@gitlab.com:slenderq/nb-ai.git -b main "$HOME/.nb/ai"
              nb use ai
              # nb sync
            '';
          })
          (pkgs.writeShellApplication {
            name = ",encrypt";
            text = ''
              gpg --pinentry-mode loopback -c keys/id_ed25519
              gpg --pinentry-mode loopback -c .env 
            '';
          })
          (pkgs.writeShellApplication {
            name = ",decrypt";
            text = ''
              gpg --pinentry-mode loopback -d keys/id_ed25519.gpg > keys/id_ed25519
              gpg --pinentry-mode loopback -d .env.gpg > .env
            '';
          })
          (pkgs.writeShellApplication {
            name = ",git_setup";
            text = ''
              #!/usr/bin/env bash
              set -e
              echo "Warning, this is meant to be run on a container, replacing git id settings"
              git config --global user.email bot@notarealemail.com
              git config --global user.name bot
            '';
          })
          (pkgs.writeShellApplication {
            name = ",run_debug_tests";
            text = ''
              #!/usr/bin/env bash
              set -e
              nix develop -c pytest -o log_cli=true -o log_cli_level=DEBUG
            '';
          })




        ];
      in
      rec {
        packages = {
          # https://ryantm.github.io/nixpkgs/languages-frameworks/python/#buildpythonpackage-function
          # https://github.com/nix-community/poetry2nix/blob/master/docs/edgecases.md
          friendly-robots = mkPoetryApplication {
            projectDir = self;
            overrides = poetryOverrides;
            preferWheels = true;
          };
          default = self.packages.${system}.friendly-robots;
        };

        # defaultPackage = packages.friendly-robots;
        devShells = {
          poetry = pkgs.mkShell {
            inputsFrom = [ self.packages.${system}.friendly-robots ];
            # nativeBuildInputs = appPkgs ++ customPkgs ++ customScripts ++ [ appEnv ];
            nativeBuildInputs = appPkgs ++ customScripts ++ [ appEnv ];
            # TODO: create a dev shell that give you the right endpoint 
            # shellHook = ''
            # export OLLAMA_HOST=http://localhost:11434
            # '';
          };
        };

        devShell = devShells.poetry;

      });
}




