CREATE EXTENSION vector;
CREATE TABLE contract(
  ContractID TEXT PRIMARY KEY,
  TomlRaw TEXT NOT NULL,
  Hash TEXT NOT NULL,
  FileContent TEXT NOT NULL,
  embedding vector(768) 
);
CREATE TABLE bills(
  BillID TEXT PRIMARY KEY,
  ContractID TEXT NOT NULL REFERENCES contract(ContractID),
  BillAuthor TEXT NOT NULL,
  BillContent TEXT NOT NULL,
  Votes INTEGER DEFAULT 1,
  embedding vector(768) 
);
