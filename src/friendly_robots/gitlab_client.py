import gitlab
import os


def get_secret():
    path = os.environ.get("GITLAB_SECRET_PATH", "")
    if path == "":
        path = "./secrets/gitlab.secret"
    if os.path.exists(path):
        with open(path) as f:
            secret = f.read().rstrip()
    else:
        raise RuntimeError(
            f"Please make sure GITLAB_SECRET_PATH is correct. Current value: {path}"
        )
    return secret


def get_gitlab():
    # private token or personal token authentication (GitLab.com)
    token = get_secret()
    gl = gitlab.Gitlab(private_token=token)
    gl.auth()
    return gl


# https://python-gitlab.readthedocs.io/en/stable/gl_objects/merge_requests.html
# def create_merege_request():
# mr = project.mergerequests.create({'source_branch': 'cool_feature',
#                                  'target_branch': 'main',
#                                  'title': 'merge cool feature',
#                                  'labels': ['label1', 'label2']})

#  mr.description = 'New description'
# mr.labels = ['foo', 'bar']
# mr.save()

# mr.merge()

# Schedule a MR to merge after the pipeline(s) succeed:

# mr.merge(merge_when_pipeline_succeeds=True)
