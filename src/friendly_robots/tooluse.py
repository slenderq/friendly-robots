"""A monad for the bot to use to process contracts"""

import subprocess
import time
import random

import re

from random import choices

from git import Repo
from git.exc import GitCommandError
import backoff

import os
import logging


class SubprocessError(Exception):
    def __init__(self, message, stdout, stderr):
        super().__init__(message)
        self.stdout = stdout
        self.stderr = stderr


def generate_id(n=64) -> str:
    """Create a human-readable identifier n characters long

    Human-readable as the data is expected to be reviewed by individuals

    169,870,458,651,940,814,848 possible ids

    len(visual_chars) Choose N possible ids when n is 64
    """
    # CONSIDERATION: In a real-world scenario, a more formal identifier may be necessary.
    # In such cases, thorough research would be conducted to determine the most suitable identifier that can scale correctly for an increasing number of Home ids.
    # Reference: https://blog.devtrovert.com/p/how-to-generate-unique-ids-in-distributed
    visual_chars = [
        "a",
        "b",
        "c",
        "d",
        "e",
        "f",
        "h",
        "i",
        "j",
        "k",
        "m",
        "n",
        "o",
        "p",
        "r",
        "s",
        "t",
        "w",
        "x",
        "y",
        "3",
        "4",
        "7",
    ]
    return "".join(choices(visual_chars, k=n))

def catch_invalid_contract(contract_id, tomls=None):
    """deal with the error contract, ideally without losing it content
    maybe post an issue"""
    ...

def test_models():
    """Do a bootstrap and download the models that is required"""
    out = subprocess.run(
        [",test_models"],  # defiend in flake.nix
        capture_output=True,
        text=True,
    )


def nb_sync():
    """Sync to the repo, with random waits to get bots of of sync and avoid reads at the same time."""

    # randomly wait a bit to make sure you don't conflict with other bots
    # full minute should be enough to make the conflicts more rare?
    wait_b = random.uniform(0.1, 3)
    wait = wait_b * wait_b
    logging.info(f"Syncing to remote in {wait} seconds ")
    time.sleep(wait)
    o = subprocess.run(["nb", "ai:sync"], capture_output=True, text=True)
    logging.debug(o.stdout)
    logging.debug(o.stderr)


def nb_setup():
    o = subprocess.run([",nb_setup"], capture_output=True, text=True)

    logging.debug(o.stdout)
    logging.debug(o.stderr)


def nb_list_contracts():
    # "contract:.*"
    ...


def remove_color(text):
    return re.sub(r"\x1B\[[0-9;]*[JKmsu]", "", text)


def parse_nb_item_int(output) -> int:
    """Parse a single output of nb into an int"""
    if not output:
        RuntimeError("No line to parse")
    number_pattern = r"\[(?:ai:)?(\d+)\]"
    # Extract the number using regular expression
    number_match = re.search(number_pattern, output)
    # Check if the number is found and extract it
    if number_match:
        nb_id = number_match.group(1)
    else:
        raise RuntimeError(f"no match {number_match}, {output}")
    return int(nb_id)


def nb_edit(nb_id, content) -> None:

    first = True
    for line in content.splitlines():

        if first:
            first = False
            subprocess.run(
                ["nb", "ai:edit", "--overwrite", str(nb_id), "-c", line],
                capture_output=True,
                text=True,
            )
            continue

        subprocess.run(
            ["nb", "ai:edit", str(nb_id), "-c", line], capture_output=True, text=True
        )


@backoff.on_exception(backoff.expo, RuntimeError, max_time=60)
def nb_add(title="", content=""):
    title_args = []

    if title != "":
        title_args = ["--title", title]

    # Down use spaces in filenames
    title = title.replace(" ", "")

    with open("/tmp/content.txt", "w") as fp:
        fp.write(content)

    command = ["nb", "ai:import"] + title_args + ["/tmp/content.txt"]
    out = subprocess.run(
        command,
        capture_output=True,
        text=True,
    )
    logging.debug(out.stdout)
    logging.debug(out.stderr)
    nb_sync()

    # broken return id parsing
    # more useful but not needed for the bot
    # output = remove_color(out.stdout.rstrip())

    # if not output:
    #     raise RuntimeError("No output for adding to nb! out: \n {output}")

    # items = parse_nb_item_int(output)

    # return items


def nb_list(pattern) -> list[int]:
    """Given a pattern return a list of int ids"""

    nb_sync()
    # out = subprocess.run(
    #  ["nb", "ai:search", "-l", pattern], capture_output=True, text=True
    # )
    out = subprocess.run(["nb", "ai:ls", "-a", pattern], capture_output=True, text=True)
    logging.debug(out.stdout)
    logging.debug(out.stderr)

    output = remove_color(out.stdout.rstrip())

    logging.debug(output)

    items = []

    for line in output.splitlines():
        try:
            if line:
                items.append(parse_nb_item_int(line))
        except RuntimeError as e:
            print(line)
            print(e)
            continue

    return items


def nb_open(nb_id) -> str:
    logging.info(nb_id)
    text = subprocess.run(
        ["nb", "ai:show", str(nb_id), "--path", "--no-color"],
        capture_output=True,
        text=True,
    )
    logging.info(text)
    path = text.stdout.rstrip()
    logging.info(path)

    with open(path) as f:
        return f.read()


def nb_rm(nb_id):
    logging.info(f"removing note {nb_id}")
    subprocess.run(["nb", "ai:rm", str(nb_id), "-y"], capture_output=True, text=True)
    nb_sync()


def run_shell_command(command: str) -> None:
    """Run a shell command."""
    subprocess.run(command, shell=True)


def git_commit(patch: str) -> None:
    """Commit changes to a git repository."""
    result = subprocess.run(["git", "commit", "-m", patch], capture_output=True)
    if result.returncode != 0:
        raise SubprocessError("Git commit failed", result.stdout, result.stderr)


def glab_mr_create(title: str, description: str) -> None:
    """Create a merge request using glab."""
    subprocess.run(["glab", "mr", "create", "-a", "-d", description, "-y"])


def validate_git_patch(git_patch: str) -> bool:
    try:
        subprocess.run(["git", "apply", "--check", git_patch], check=True)
        return True
    except subprocess.CalledProcessError:
        raise SubprocessError("Git patch failed", result.stdout, result.stderr)


def process_contract(contract_toml: dict) -> None:
    # """Process a contract defined in a TOML file."""
    #
    if validate_git_patch(contract_toml["git_patch"]):
        git_commit(contract_toml["git_patch"])
        glab_mr_create(contract_toml["mr_title"], contract_toml["mr_description"])


def git_clone(url, clone_dir=None) -> str:
    """Checkout a Repo, default of tmp directory

    returns the path to the repo that was checked out
    """
    if clone_dir == None:
        tmpid = generate_id(n=20)
        clone_dir = f"/tmp/{tmpid}"

    logging.info(f"cloning: {url} to {clone_dir}")

    try:
        Repo.clone_from(url, clone_dir)
    except GitCommandError as e:
        raise FileNotFoundError(e)  # make the error more general

    return clone_dir
