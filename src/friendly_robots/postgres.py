"""Adaptor Module for postgres"""

from typing import Union

# psql -h localhost -p 15432 -U your_username -d your_database
import os
import logging

import numpy as np
import pprint
import psycopg2
import hashlib
from pgvector.psycopg2 import register_vector
from friendly_robots.utils import generate_id
from friendly_robots.ollama_client import embeddings_np_array


def connect():
    """Get a connection object for this database"""

    password = os.getenv("POSTGRES_PASSWORD", "admin")
    host = os.getenv("POSTGRES_HOST", "localhost")
    conn = psycopg2.connect(
        host=host, port=5432, dbname="postgres", user="postgres", password=password
    )
    # pgvector
    register_vector(conn)
    return conn


def get_bills(contract_object_id: str) -> list[dict[str, Union[str, int]]]:
    conn = connect()
    cur = conn.cursor()

    cur.execute(
        "SELECT ContractID, BillAuthor, BillContent, Votes FROM bills WHERE ContractID = %s",
        (contract_object_id,),
    )

    results = cur.fetchall()

    cur.close()
    conn.close()

    bills = []

    for r in results:
        id, author, content, votes = r
        i = {"author": author, "content": content, "votes": votes}
        bills.append(i)

    logging.debug(f"contract: {contract_object_id} bills: \n {pprint.pformat(bills)}")

    return bills


def add_bill(bill, author, contract_object_id) -> str:
    conn = connect()
    cur = conn.cursor()

    new_id = generate_id(n=200)  # 200 should be enough for no collitions

    embedding = embeddings_np_array(bill)

    # default of 1 for votes column
    cur.execute(
        "INSERT INTO bills VALUES (%s, %s, %s, %s, 1, %s);",
        (new_id, contract_object_id, author, bill, embedding),
    )
    conn.commit()

    logging.info(f"adding bill by {author} to the database")

    cur.close()
    conn.close()

    return new_id


def register_contract(c):

    id = c.id

    conn = connect()
    cur = conn.cursor()

    raw_toml = c.to_tomls()

    hash_object = hashlib.sha256(raw_toml.encode())
    hash_hex = hash_object.hexdigest()

    cur.execute(
        "SELECT ContractID FROM contract WHERE Hash = %s",
        (hash_hex,),
    )

    if cur.rowcount > 0:
        logger.debug("Found a similar contract, using the same id")
        res = cur.fetchone()
        id = res[0]

    # don't worry about our take_slice reading in this case
    with open(c.content_path, "r") as f:
        content = f.read()
    embedding = embeddings_np_array(str(content))

    cur.execute(
        "INSERT INTO contract (ContractID, TomlRaw, Hash, FileContent, embedding) VALUES (%s, %s, %s, %s, %s) ON CONFLICT (ContractID) DO UPDATE SET embedding = EXCLUDED.embedding;",
        (id, raw_toml, hash_hex, content, embedding),
    )
    # cur.execute("INSERT INTO files VALUES (%s, %s, %s, %s);", (c.id, raw_toml, content, embedding))
    conn.commit()

    logging.info(f"adding contract {c.id} to the database")

    cur.close()
    conn.close()

    return id


def get_similar_contracts(c, results=5) -> list[str]:
    """Get the 'results' number of similar contracts to a given contract"""
    conn = connect()
    cur = conn.cursor()

    request_id = c.id
    q = "SELECT ContractID FROM contract WHERE ContractID != %s ORDER BY embedding <-> (SELECT embedding FROM contracts WHERE ContractID = %s) LIMIT %s;"

    cur.execute(q, (c.id, c.id, str(results)))
    res = cur.fetchall()

    out = [x[0] for x in res]  # unroll the tuples of the result

    cur.close()
    conn.close()

    return out
