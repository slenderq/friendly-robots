"""Module for utility functions"""

import numpy as np
from typing import Union
from friendly_robots.ollama_client import embeddings
from sklearn.metrics.pairwise import cosine_similarity
from random import choices


def find_best_bill(
    bills: list[dict[str, Union[int, str]]]
) -> dict[str, Union[int, str]]:
    """Based on the votes and answers find the best bill that we want to consider"""

    best_idx = -1
    best_score = -999999999.0
    total_scores = []
    embeds = []

    # build a list of embeddings
    for b in bills:
        weight = b.get("votes", 1)
        e = embeddings(b.get("content"))
        for i in range(0, weight):
            embeds.append(e.get("embedding"))

    # https://scikit-learn.org/stable/modules/generated/sklearn.metrics.pairwise.cosine_similarity.html

    for i, canidate in enumerate(embeds):
        scores = []

        # calculate pairwise similarity scores
        for e in embeds:
            if canidate == e:
                continue
            sim_score = cosine_similarity([canidate, e], dense_output=False)
            scores.append(sim_score)

        avg_score = float(np.average(scores))

        if avg_score > best_score:
            best_score = avg_score
            best_idx = i

        total_scores.append(avg_score)

    print(total_scores)

    return bills[best_idx]

    # consensus = np.average(embeds, axis=1, weights=weights)

    # I would like to find the similarity and then find the bill that is most similar


def generate_id(n=200) -> str:
    """Create a human-readable identifier n characters long

    Human-readable as the data is expected to be reviewed by individuals

    169,870,458,651,940,814,848 possible ids

    len(visual_chars) Choose N possible ids when n is 64
    """
    # CONSIDERATION: In a real-world scenario, a more formal identifier may be necessary.
    # In such cases, thorough research would be conducted to determine the most suitable identifier that can scale correctly for an increasing number of Home ids.
    # Reference: https://blog.devtrovert.com/p/how-to-generate-unique-ids-in-distributed
    visual_chars = [
        "a",
        "b",
        "c",
        "d",
        "e",
        "f",
        "h",
        "i",
        "j",
        "k",
        "m",
        "n",
        "o",
        "p",
        "r",
        "s",
        "t",
        "w",
        "x",
        "y",
        "3",
        "4",
        "7",
    ]
    return "".join(choices(visual_chars, k=n))


"""
I wonder if there should be a bill that is created based on the combined answers of other bots.

that whole bots job is to take solutions and make hybrid

"""
