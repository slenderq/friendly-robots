import ollama
import numpy as np
from typing import Optional
import os
import httpx
import backoff

import logging

# TODO: configure this port better


@backoff.on_exception(backoff.expo, httpx.ConnectError, max_time=60)
def call_llm(
    prompt: str, system: str, model: Optional[str] = None, **option_kwags
) -> str:
    # https://github.com/ollama/ollama/blob/main/docs/modelfile.md#valid-parameters-and-values
    assert prompt != ""
    if not model:
        model = os.getenv("OLLAMA_MODEL", "codellama:7b")
    OLLAMA_URL = os.getenv("OLLAMA_HOST", "http://localhost:11434")
    print(f"Calling model: {model}, on host {OLLAMA_URL}")
    logging.info(f"Connecting to {OLLAMA_URL}")
    client = ollama.Client(host=OLLAMA_URL)
    options = {**option_kwags}

    response = client.chat(
        model=model,
        messages=[
            {"role": "system", "content": system},
            {
                "role": "user",
                "content": prompt,
            },
        ],
        options=options,
    )
    content = response.get("message").get("content")

    return content


# TODO: use this to add an embedding of the bill output
#  this will help later when we average them.
# it also means we can create a "standing average"
def embeddings(prompt):
    return ollama.embeddings(model="nomic-embed-text", prompt=prompt)


def embeddings_np_array(prompt) -> np.array:
    e = embeddings(prompt=prompt)
    embedding = np.array(e["embedding"])
    return embedding
