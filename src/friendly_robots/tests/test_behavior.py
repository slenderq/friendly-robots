from friendly_robots.contract import Contract, new_contract
from friendly_robots.bot import Bot, BotShutdown, BotCrash
import pytest
import httpx
from flaky import flaky


@pytest.fixture(autouse=True)
def set_ollama_host_env_var():
    """Make sure we can locally and connect to the docker ollama instance"""
    import os

    os.environ["OLLAMA_HOST"] = "localhost:11434"


try:
    from friendly_robots.ollama_client import call_llm
except httpx.ConnectError:
    pytest.mark.skip()

red = new_contract(
    "Find the red herring and output where its located",
    "single string",
    "README.md",
    repo="http://gitlab.com/slenderq/friendly-robots.git",
    reusable=False,
    skip_mr=False,
    no_nb=True,
)


def test_dummy_smoke():
    """Validate that the bot logic works and doesn't crash in an unexpected way"""
    # run with dummy mode on
    llm_bot = Bot()
    llm_bot.receive_contract(red)
    llm_bot.dummy = True
    try:
        llm_bot.start()
    except (SystemExit, BotShutdown):
        pass  # This is a expected exit of the bot when it runs out of the actions
    except BotCrash as e:
        print("The Bot crashed in an unexpected way")
        raise e


@flaky(max_runs=3)
def test_yes_no_question_answer():

    memory = "There is a blue herring and a red herring in the pond"
    # Example of how to use the LLMBot class
    llm_bot = Bot()

    # I know kung fu!?!
    llm_bot.memory = memory
    llm_bot.values = "You are a friendly repo robot who is proitizes accuracy."

    llm_bot.receive_contract(red)

    answer = llm_bot.yes_no_llm_call("Do you know about the red herring")

    assert answer


@flaky(max_runs=3)
def test_yes_no_question_answer_inverse():

    memory = "There is a blue duck in the pond"
    # Example of how to use the LLMBot class
    llm_bot = Bot()

    # I know kung fu!?!
    llm_bot.memory = memory
    llm_bot.values = "You are a friendly repo robot who is proitizes accuracy."

    llm_bot.receive_contract(red)

    answer = llm_bot.yes_no_llm_call("Is there is a red herring in the pond")

    assert not answer


@pytest.mark.skip
def test_solve_contract():
    llm_bot = Bot()
    contract = Contract.from_toml("./contracts/gather-test.toml")
    llm_bot.receive_contract(contract)
    memory = """
It appears that the code in this file is a Python script that defines a Telegram bot using the
`telegram` library. The bot will respond to messages from users by repeating them. The code inc
ludes some basic error handling and logging functionality, as well as a few helper functions fo
r interacting with a SQLite database.

The main entry point of the program is the `get_secret()` function, which retrieves the secret
key used to authenticate the bot with Telegram. The `write_user_to_db()` function adds user inf
ormation to the database, while the `get_user_info_by_id()` function retrieves information abou
t a specific user by their ID.

The `telegram` library is imported at the top of the file, as well as other libraries such as `
os`, `logging`, and `sqlite3`. The `CommandHandler` and `MessageHandler` classes are also defin
ed in this file, which will be used to handle different types of messages received by the bot.

Overall, this code appears to be a simple implementation of a Telegram bot that can store user
information in a database and repeat back incoming messages.
"""
    # i know kung fuuuu
    llm_bot.memory = memory
    llm_bot.values = "You are a friendly repo robot who is proitizes accuracy."
    llm_bot.solve_contract(contract)
