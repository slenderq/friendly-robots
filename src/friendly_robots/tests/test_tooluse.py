from friendly_robots.tooluse import (
    nb_add,
    nb_open,
    nb_rm,
    git_clone,
    nb_list,
    nb_edit,
    generate_id,
)
import os
import pytest

MIT = """MIT License

Copyright (c) 2024 Justin Quaintance

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


@pytest.fixture(autouse=True)
def remove_testing_notes():
    cleanup_label("'testtitle'")
    cleanup_label("'DELETE'")


def cleanup_label(prefix):
    ids = nb_list(prefix)
    for i in ids:
        nb_rm(nb_id)


@pytest.mark.skip(
    reason="the return type is not working right now and is not really needed"
)
def test_nb_add():
    test_text = """
    testing!

    does this last line get added?
     """

    nb_id = nb_add("testtitle", test_text)

    returned_text = nb_open(nb_id)

    try:
        assert test_text == returned_text
    finally:
        nb_rm(nb_id)


@pytest.mark.skip(
    reason="the return type is not working right now and is not really needed"
)
def test_nb_add_spaces_title():
    test_text = """
    testing!

    does this last line get added?
     """

    nb_id = nb_add("testtitle test. test", test_text)

    returned_text = nb_open(nb_id)

    try:
        assert test_text == returned_text
    finally:
        nb_rm(nb_id)


@pytest.mark.skip(reason="going to try another plan")
def test_nb_edit():
    test_text = """
    there's a war!
     """

    nb_id = nb_add("testtitle", test_text)

    new_text = """


    no war in this city

"""

    nb_edit(nb_id, new_text)

    c_text = nb_open(nb_id)

    try:
        assert c_text == new_text
    finally:
        nb_rm(nb_id)


def test_git_clone():
    """check that I can git clone into the tmp dir"""
    expected = "hello fellow testers\n"
    test_repo = "https://gitlab.com/slenderq/friendly-robots.git"
    found_dir = git_clone(test_repo)
    print(found_dir)
    path = found_dir + "/src/friendly_robots/tests/test.file"
    with open(path) as f:
        actual = f.read()

    try:
        assert expected == actual
    finally:
        os.remove(path)


def test_git_clone_forign():
    """check that I can git clone into the tmp dir"""
    expected = MIT
    test_repo = "https://gitlab.com/slenderq/ollama-telebot.git"
    found_dir = git_clone(test_repo)
    print(found_dir)
    path = found_dir + "/LICENSE"

    with open(path) as f:
        actual = f.read()

    try:
        assert expected == actual
    finally:
        os.remove(path)


def test_nb_list():
    # assemble
    key = generate_id(n=5)
    real_ids = []
    for i in range(0, 5):
        nb_id = nb_add(f"{key} {i}", "bar")
        real_ids.append(nb_id)

    try:
        actual_ids = nb_list(f"{key}")
        assert sorted(real_ids) == sorted(actual_ids)
    finally:
        for nb_id in real_ids:
            nb_rm(nb_id)
