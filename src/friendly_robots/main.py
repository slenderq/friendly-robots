import traceback
import argparse
import os
import ollama

# from gitlab_client import get_gitlab
from src.friendly_robots.bot import PublisherBot, Bot, BotCrash
from src.friendly_robots.contract import Contract, new_contract

import logging

log_level = os.getenv("LOG_LEVEL", "INFO")
logging.basicConfig(level=log_level, format="%(asctime)s - %(levelname)s - %(message)s")


# gl = get_gitlab()


def main():
    s_publisher = os.getenv("BOT_PUBLISHER", "False")

    if "True" in s_publisher:
        bot = PublisherBot()
    else:
        bot = Bot()
    bot.start()


if __name__ == "__main__":
    main()
